module.exports = {
    entry: "./src/server/server.ts",
    target: 'node',
    output: {
        path:__dirname+ '/dist/',
        filename: "bundle.js",
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx|ts|tsx)/,
                exclude:/(node_modules|bower_components)/,
                loader: 'ts-loader',
            }
        ]
    }
};
