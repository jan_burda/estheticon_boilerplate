import * as graphql from 'graphql/type/definition';
declare module 'graphql/type/definition' {
    export interface GraphQLObjectTypeConfig<TSource, TContext> {
        uniqueKey?: string;
        sqlTable?: string;
    }
}
