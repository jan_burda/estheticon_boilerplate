import 'reflect-metadata';

import * as express from 'express';
import { createConnection } from 'typeorm';

const app = express();

app.get('/', (req, res) => {
    res.send('hoj');
    res.end();
});


createConnection().then(async connection => {
// tslint:disable-next-line:no-console
}).catch(err => console.log(err));

app.listen(3001, () => {
    // tslint:disable-next-line:no-console
    console.log('running');
});