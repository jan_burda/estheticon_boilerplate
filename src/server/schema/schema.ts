import { GraphQLSchema } from 'graphql';
import Query from './Query/Query';

const schema = new GraphQLSchema({
    query: Query
});

export default schema;
