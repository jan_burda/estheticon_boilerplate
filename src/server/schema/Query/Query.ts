import { GraphQLObjectType, GraphQLScalarType } from 'graphql';
import Resolvers from './Resolvers';
import User from '../User/User';
import joinMonster from 'join-monster';
import { GraphQLID } from 'graphql/type/scalars';
import Surgeon from '../Surgeon/Surgeon';

interface SurgeonArgs {
    id: { type: GraphQLScalarType };
}

const query = new GraphQLObjectType({
    name: 'Query',
    description: '..',
    fields: () => ({
        me: {
            type: User,
            resolve: Resolvers.me
        },
        surgeon: {
            type: Surgeon,
            args: { id: { type: GraphQLID } },
            resolve: (parent, args: SurgeonArgs, context, resolveInfo) => {
                
            }
        }
    })
});

export default query;