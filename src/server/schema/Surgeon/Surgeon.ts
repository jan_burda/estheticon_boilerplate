import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
} from 'graphql';

const surgeon = new GraphQLObjectType({
    name: 'Surgeon',
    fields: () => ({
        id: {
            type: GraphQLID
        },
        username: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString,
        },
    }),
    uniqueKey: 'id',
    sqlTable: 'user',
});

export default surgeon;
