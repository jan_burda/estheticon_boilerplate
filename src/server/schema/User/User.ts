import resolvers from './resolvers';
import { GraphQLObjectType, GraphQLID, GraphQLString } from 'graphql';

const schema = new GraphQLObjectType({
    name: 'user',
    fields: () => ({
        id: {
            type: GraphQLID,
            resolve: resolvers.id
        },
        role: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString
        }
    }),
});

export default schema;