import { inject } from '../../../services/StateService/Wrapper';
import * as React from 'react';
import { MainStore } from '../../../services/StateService/Store';

export interface IDiscussionSceneProps {

}

interface ISceneInjectedProps {

}

interface ISceneState {
}

class DiscussionScene extends React.Component<IDiscussionSceneProps & ISceneInjectedProps, ISceneState> {
    public render() {
        return (
            <div />
        );
    }
}

const mapStateToProps = (store: MainStore): ISceneInjectedProps => {
    return {

    };
};

export default inject<IDiscussionSceneProps>(mapStateToProps)(DiscussionScene);
