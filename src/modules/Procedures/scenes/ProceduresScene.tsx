import { inject } from '../../../services/StateService/Wrapper';
import * as React from 'react';
import { MainStore } from '../../../services/StateService/Store';

export interface IProceduresSceneProps {

}

interface ISceneInjectedProps {

}

interface ISceneState {
}

class ProceduresScene extends React.Component<IProceduresSceneProps & ISceneInjectedProps, ISceneState> {
    public render() {
        return (
            <div />
        );
    }
}

const mapStateToProps = (store: MainStore): ISceneInjectedProps => {
    return {

    };
};

export default inject<IProceduresSceneProps>(mapStateToProps)(ProceduresScene);
