import { inject } from '../../../services/StateService/Wrapper';
import * as React from 'react';
import { MainStore } from '../../../services/StateService/Store';

export interface ISurgeonSceneProps {

}

interface ISceneInjectedProps {

}

interface ISceneState {
}

class SurgeonScene extends React.Component<ISurgeonSceneProps & ISceneInjectedProps, ISceneState> {
    public render() {
        return (
            <div />
        );
    }
}

const mapStateToProps = (store: MainStore): ISceneInjectedProps => {
    return {

    };
};

export default inject<ISurgeonSceneProps>(mapStateToProps)(SurgeonScene);
