import { inject } from '../../../services/StateService/Wrapper';
import * as React from 'react';
import { MainStore } from '../../../services/StateService/Store';
import { VideoPlayer } from '../partials/VideoPlayer';

export interface IPatientsSceneProps {

}

interface ISceneInjectedProps {

}

interface ISceneState {
}

class VideoScene extends React.Component<IPatientsSceneProps & ISceneInjectedProps, ISceneState> {
    public render() {
        return (
            <div>
                <VideoPlayer videoUrl={'https://vimeo.com/240140745'} />
            </div>
        );
    }
}

const mapStateToProps = (store: MainStore): ISceneInjectedProps => {
    return {

    };
};

export default inject<IPatientsSceneProps>(mapStateToProps)(VideoScene);
