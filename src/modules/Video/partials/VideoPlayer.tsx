import * as React from 'react';
import ReactPlayer from 'react-player';

export interface IVideoPlayerProps {
    videoUrl: string;
}

export class VideoPlayer extends React.PureComponent<IVideoPlayerProps> {
    public render() {
        return (
            <ReactPlayer url={this.props.videoUrl} />
        );
    }
}