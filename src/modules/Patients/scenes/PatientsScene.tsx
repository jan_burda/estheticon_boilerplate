import { inject } from '../../../services/StateService/Wrapper';
import * as React from 'react';
import { MainStore } from '../../../services/StateService/Store';

export interface IPatientsSceneProps {

}

interface ISceneInjectedProps {

}

interface ISceneState {
}

class PatientsScene extends React.Component<IPatientsSceneProps & ISceneInjectedProps, ISceneState> {
    public render() {
        return (
            <div />
        );
    }
}

const mapStateToProps = (store: MainStore): ISceneInjectedProps => {
    return {

    };
};

export default inject<IPatientsSceneProps>(mapStateToProps)(PatientsScene);
