import { inject } from '../../../services/StateService/Wrapper';
import * as React from 'react';
import { MainStore } from '../../../services/StateService/Store';

export interface IMainSceneProps {

}

interface IMainSceneInjectedProps {

}

interface IMainSceneState {
}

class MainScene extends React.Component<IMainSceneProps & IMainSceneInjectedProps, IMainSceneState> {
    public render() {
        return (
            <div />
        );
    }
}

const mapStateToProps = (store: MainStore): IMainSceneInjectedProps => {
    return {

    };
};

export default inject<IMainSceneProps>(mapStateToProps)(MainScene);
