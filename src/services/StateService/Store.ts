
export class MainStore {
    public static instance: MainStore;

    public set Test(val: string) {
        this._test = val;
        this.RefreshAll();
    }

    public get Test(): string {
        return this._test;
    }

    public set AnotherTest(val: string) {
        this._anotherTest = val;
        this.RefreshAll();
    }

    public get AnotherTest(): string {
        return this._anotherTest;
    }

    private _test: string;
    private _anotherTest: string;

    private subscribers: Array<() => void> = new Array();

    public static getInstance() {
        if (this.instance === undefined) {
            this.createInstance();
        }

        return this.instance;
    }

    private static createInstance() {
        this.instance = new MainStore();
    }

    public Subscribe(handleChange: () => void) {
        this.subscribers.push(handleChange);
        return () => this.Unsubscribe(handleChange);
    }

    public Unsubscribe(handleChange: () => void) {
        this.subscribers = this.subscribers.filter(s => s !== handleChange);
    }

    public RefreshAll() {
        this.subscribers.map(s => s());
    }
}