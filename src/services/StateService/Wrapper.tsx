import { MainStore } from './Store';
import * as React from 'react';

export const inject = <TProps extends {}>(mapStateToProps: (store: MainStore) => Object) =>
    // tslint:disable-next-line:no-any
    (WrappedComponent: any) => {
        return class extends React.Component<TProps> {
            private unsubscribe: () => void;

            componentDidMount() {
                this.unsubscribe = MainStore.getInstance().Subscribe(this.handleChange.bind(this));
            }

            handleChange() {
                this.forceUpdate();
            }

            componentWillUnmount() {
                this.unsubscribe();
            }

            render() {
                return (
                    <WrappedComponent
                        {...this.props}
                        {...mapStateToProps(MainStore.getInstance())}
                    />
                );
            }
        };
    };