import VideoScene from './modules/Video/scenes/VideoScene';
import * as React from 'react';

interface IAppInjectedProps {
    t?: string;
}

interface IAppProps {
}

export default class App extends React.PureComponent<IAppProps & IAppInjectedProps> {
    render() {
        return (
            <VideoScene />
        );
    }
}